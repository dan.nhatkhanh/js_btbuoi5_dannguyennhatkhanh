function tinhTienDien() {
  const hoTen = document.getElementById(`hoten`).value;
  const soKw = document.getElementById(`sokw`).value * 1;

  var giaKw50Dau = 500;
  var giaKw50Ke = 650;
  var giaKw100Ke = 850;
  var giaKw150Ke = 1100;
  var giaKwConLai = 1300;
  var result = 0;

  if (soKw != "" && hoTen != "") {
    if (0 <= soKw && soKw <= 50) {
      result = soKw * giaKw50Dau;

      document.getElementById(
        `result-ex2`
      ).innerHTML = `Khách hàng ${hoTen} phải trả  ${Intl.NumberFormat(
        "vn-VN",
        {
          style: "currency",
          currency: "VND",
        }
      ).format(result)} tiền điện`;
    } else if (50 < soKw && soKw <= 100) {
      result = 50 * giaKw50Dau + (soKw - 50) * giaKw50Ke;

      document.getElementById(
        `result-ex2`
      ).innerHTML = `Khách hàng ${hoTen} phải trả  ${Intl.NumberFormat(
        "vn-VN",
        {
          style: "currency",
          currency: "VND",
        }
      ).format(result)} tiền điện`;
    } else if (100 < soKw && soKw <= 200) {
      result = 50 * giaKw50Dau + 50 * giaKw50Ke + (soKw - 100) * giaKw100Ke;

      document.getElementById(
        `result-ex2`
      ).innerHTML = `Khách hàng ${hoTen} phải trả  ${Intl.NumberFormat(
        "vn-VN",
        {
          style: "currency",
          currency: "VND",
        }
      ).format(result)} tiền điện`;
    } else if (200 < soKw && soKw <= 350) {
      result =
        50 * giaKw50Dau +
        50 * giaKw50Ke +
        100 * giaKw100Ke +
        (soKw - 200) * giaKw150Ke;

      document.getElementById(
        `result-ex2`
      ).innerHTML = `Khách hàng ${hoTen} phải trả  ${Intl.NumberFormat(
        "vn-VN",
        {
          style: "currency",
          currency: "VND",
        }
      ).format(result)} tiền điện`;
    } else if (350 < soKw) {
      result =
        50 * giaKw50Dau +
        50 * giaKw50Ke +
        100 * giaKw100Ke +
        150 * giaKw150Ke +
        (soKw - 350) * giaKwConLai;

      document.getElementById(
        `result-ex2`
      ).innerHTML = `Khách hàng ${hoTen} phải trả  ${Intl.NumberFormat(
        "vn-VN",
        {
          style: "currency",
          currency: "VND",
        }
      ).format(result)} tiền điện`;
    } else {
      document.getElementById(
        `result-ex2`
      ).innerHTML = `Vui lòng kiểm tra thông tin!`;
    }
  } else {
    document.getElementById(
      `result-ex2`
    ).innerHTML = `Vui lòng kiểm tra thông tin!`;
  }

  $("#form-2").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
}
