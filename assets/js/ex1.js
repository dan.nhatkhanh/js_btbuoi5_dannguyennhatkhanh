const viTriA = "2";
const viTriB = "1";
const viTriC = "0.5";

function congKhuVuc(place) {
  var diemKhuVuc = 0;
  if (place == viTriA) {
    diemKhuVuc = 2;
  }
  if (place == viTriB) {
    diemKhuVuc = 1;
  }
  if (place == viTriC) {
    diemKhuVuc = 0.5;
  }
  return diemKhuVuc;
}

const doiTuong1 = "2.5";
const doiTuong2 = "1.5";
const doiTuong3 = "1";

function congDoiTuong(subject) {
  var diemDoiTuong = 0;
  if (subject == doiTuong1) {
    diemDoiTuong = 2.5;
  }
  if (subject == doiTuong2) {
    diemDoiTuong = 1.5;
  }
  if (subject == doiTuong3) {
    diemDoiTuong = 1;
  }
  return diemDoiTuong;
}

function kiemTra() {
  var a = document.getElementById(`khuvuc`);
  var khuVucValue = a.options[a.selectedIndex].value;

  var b = document.getElementById(`doituong`);
  var doiTuongValue = b.options[b.selectedIndex].value;

  var xetKhuVuc = congKhuVuc(khuVucValue);
  var xetDoiTuong = congDoiTuong(doiTuongValue);
  console.log("xetDoiTuong: ", xetDoiTuong);

  const diemChuan = document.getElementById(`diemchuan`).value * 1;
  const diemMon1 = document.getElementById(`mon1`).value * 1;
  const diemMon2 = document.getElementById(`mon2`).value * 1;
  const diemMon3 = document.getElementById(`mon3`).value * 1;

  var result = diemMon1 + diemMon2 + diemMon3 + xetKhuVuc + xetDoiTuong;

  if (diemChuan != "" && diemMon1 != "" && diemMon2 != "" && diemMon3 != "") {
    if (0 < result && result < diemChuan) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `Bạn đã rớt. Tổng điểm: ${result}`;
    } else if (result >= diemChuan) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `Bạn đã đậu. Tổng điểm: ${result}`;
    } else {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `Vui lòng kiểm tra thông tin!`;
    }
  } else {
    document.getElementById(
      `result-ex1`
    ).innerHTML = `Vui lòng kiểm tra thông tin!`;
  }

  $("#form-1").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
}
